module.exports = {
  "env": {
    "browser": true,
    "commonjs": true,
    "es6": true,
    "jquery": true
  },
  "globals": {
    "toastr": false
  },
  "extends": "standard"
};
