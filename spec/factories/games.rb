# == Schema Information
#
# Table name: games
#
#  access_token :string           not null
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  question     :string           not null
#  title        :string           not null
#  updated_at   :datetime         not null
#  video_id     :integer          not null
#

FactoryBot.define do
  factory :game do
    video_id FFaker::IdentificationBR.cnpj
    title FFaker::CheesyLingo.title
    question FFaker::LoremIE.question
    access_token FFaker::Code.ean
  end
end
