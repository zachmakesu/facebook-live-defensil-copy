require "rails_helper"

RSpec.describe SaveCommentJob, type: :job do
  let(:game) { create(:game) }
  let(:data) do
    {
      "data" => {
        "message" => FFaker::BaconIpsum.word,
        "from" => {
          "id" => FFaker::IdentificationBR.cnpj,
          "name" => FFaker::Name.name,
          "picture" => {
            "data" => {
              "url" => FFaker::Avatar.image
            }
          }
        }
      }.to_json,
      "game_id" => game.id
    }
  end

  before(:each) do
    SaveCommentJob.perform_now data
    data["data"] = JSON.parse(data["data"]).with_indifferent_access
  end

  context "new user" do
    it { expect(Viewer.last.name).to eq(data["data"]["from"]["name"]) }
    it { expect(Comment.last.message).to eq(data["data"]["message"]) }
  end
end
