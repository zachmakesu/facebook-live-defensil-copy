Rails.application.routes.draw do
  mount ActionCable.server => "/cable"
  mount Sidekiq::Web => "/sidekiq" # monitoring console
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :admins, path: "admins", controllers: { sessions: "admins/sessions" }, skip: %i[registrations password]
  authenticated :admin do
    root "admins/games#index", as: :authenticated_admin_root
    namespace :admins do
      resources :games do
        member do
          patch "toggle_fetching", to: "games#toggle_fetching"
        end
      end
    end
  end
  resources :games, only: %i[show]
  root "home#index"
end
