ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require 'bundler/setup' # Set up gems listed in the Gemfile.
require 'bootsnap'
# https://github.com/Shopify/bootsnap/issues/103#issuecomment-352206807
env = ENV['RAILS_ENV'] || 'development'
bb_pipelines_off = ENV['BBPIPELINESCI'].nil?
Bootsnap.setup(
  cache_dir: 'tmp/cache',
  development_mode: env == 'development',
  load_path_cache: bb_pipelines_off,
  autoload_paths_cache: bb_pipelines_off,
  disable_trace: true,
  compile_cache_iseq: true,
  compile_cache_yaml: true
)
