source "https://rubygems.org"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby "2.5.1"

gem "active_type", ">= 0.3.2"
gem "autoprefixer-rails", ">= 5.0.0.1"
gem "bcrypt", "~> 3.1.7"
gem "bootsnap", ">= 1.2.0", require: false
gem "bootstrap-sass", "~> 3.3", ">= 3.3.7"
gem "cocoon"
gem "coffee-rails", "~> 4.2"
gem "devise"
gem "dotenv-rails", ">= 2.0.0"
gem "draper"
gem "ffaker"
gem "font-awesome-rails"
gem "ionicons-rails", "~> 2.0"
gem "jquery-rails"
gem "kaminari"
gem "mail", ">= 2.6.3"
gem "marco-polo"
gem "pg", ">= 0.18"
gem "pgcli-rails"
gem "rails", "~> 5.2.0"
gem "sass-rails", "~> 5.0"
gem "secure_headers", "~> 3.0"
gem "sidekiq", ">= 4.2.0"
gem "simple_form"
gem "toastr-rails"
gem "turbolinks", "~> 5"
gem "typhoeus"

group :production, :staging do
  gem "postmark-rails"
  gem "unicorn"
  gem "unicorn-worker-killer"
end

group :development, :test do
  gem "factory_bot_rails"
  gem "pry"
  gem "rails-controller-testing"
  gem "rspec-rails", "~> 3.7"
end

group :development do
  gem "annotate", ">= 2.5.0"
  gem "awesome_print"
  gem "bcrypt_pbkdf", require: false
  gem "better_errors"
  gem "binding_of_caller"
  gem "brakeman", require: false
  gem "bundler-audit", ">= 0.5.0", require: false
  gem "capistrano", "~> 3.10", require: false
  gem "capistrano-bundler", "~> 1.3", require: false
  gem "capistrano-mb", ">= 0.33.0", require: false
  gem "capistrano-nc", require: false
  gem "capistrano-rails", require: false
  gem "capistrano-rbenv", "~> 2.1"
  gem "guard", ">= 2.2.2", require: false
  gem "guard-livereload", require: false
  gem "guard-minitest", require: false
  gem "letter_opener"
  gem "listen", ">= 3.0.5"
  gem "overcommit", ">= 0.37.0", require: false
  gem "puma", "~> 3.11"
  gem "rack-livereload"
  gem "rbnacl", "~> 3.4", require: false
  gem "rbnacl-libsodium", require: false
  gem "rubocop", ">= 0.53.0", require: false
  gem "scss_lint", require: false
  gem "simplecov", require: false
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "sshkit", "~> 1.16", require: false
  gem "terminal-notifier", require: false
  gem "terminal-notifier-guard", require: false
  gem "xray-rails", ">= 0.1.18"
end

group :test do
  gem "capybara", ">= 2.15"
  gem "launchy"
  gem "minitest-ci", ">= 3.3.0", require: false
  gem "mocha", ">= 1.4.0"
  gem "selenium-webdriver"
  gem "shoulda-context"
  gem "shoulda-matchers", ">= 3.0.1"
end
