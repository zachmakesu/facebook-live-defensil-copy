class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.integer :game_id
      t.string  :words
      t.timestamps
    end
  end
end
