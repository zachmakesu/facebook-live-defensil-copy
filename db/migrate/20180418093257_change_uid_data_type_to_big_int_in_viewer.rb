class ChangeUidDataTypeToBigIntInViewer < ActiveRecord::Migration[5.2]
  def change
    change_column :viewers, :uid, :integer, limit: 5, null: false
  end
end
