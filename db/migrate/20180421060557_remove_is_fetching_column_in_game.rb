class RemoveIsFetchingColumnInGame < ActiveRecord::Migration[5.2]
  def change
    remove_column :games, :is_fetching, :boolean
  end
end
