class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.integer :video_id, null: false
      t.string  :title, null: false
      t.string  :question, null: false
      t.timestamps
    end
  end
end
