class DropReactionTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :reactions
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
