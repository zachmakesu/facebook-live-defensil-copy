class AddGameIdToCorrectAnswer < ActiveRecord::Migration[5.2]
  def change
    add_column :correct_answers, :game_id, :integer
  end
end
