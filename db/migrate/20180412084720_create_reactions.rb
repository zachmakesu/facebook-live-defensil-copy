class CreateReactions < ActiveRecord::Migration[5.2]
  def change
    create_table :reactions do |t|
      t.integer :viewer_id
      t.integer :game_id
      t.string  :reaction_type
      t.timestamps
    end
  end
end
