class RenameWordsToMessageInComment < ActiveRecord::Migration[5.2]
  def change
    rename_column :comments, :words, :message
  end
end
