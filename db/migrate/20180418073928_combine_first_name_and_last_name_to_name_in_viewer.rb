class CombineFirstNameAndLastNameToNameInViewer < ActiveRecord::Migration[5.2]
  def change
    rename_column :viewers, :first_name, :name
    remove_column :viewers, :last_name
  end
end
