class CreateViewers < ActiveRecord::Migration[5.2]
  def change
    create_table :viewers do |t|
      t.integer :uid, null: false
      t.string :first_name
      t.string :last_name
      t.string :profile_photo
      t.timestamps
    end
  end
end
