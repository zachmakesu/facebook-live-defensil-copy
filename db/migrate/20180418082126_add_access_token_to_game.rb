class AddAccessTokenToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :access_token, :string, null: false
  end
end
