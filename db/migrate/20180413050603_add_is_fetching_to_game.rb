class AddIsFetchingToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :is_fetching, :boolean, default: false
  end
end
