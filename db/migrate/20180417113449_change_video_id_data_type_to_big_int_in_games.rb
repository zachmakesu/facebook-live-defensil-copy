class ChangeVideoIdDataTypeToBigIntInGames < ActiveRecord::Migration[5.2]
  def change
    change_column :games, :video_id, :integer, limit: 5, null: false
  end
end
