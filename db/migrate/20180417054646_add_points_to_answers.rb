class AddPointsToAnswers < ActiveRecord::Migration[5.2]
  def change
    add_column :answers, :points, :integer, default: 0
  end
end
