class CreateCorrectAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :correct_answers do |t|
      t.integer :answer_id
      t.integer :comment_id
      t.timestamps
    end
  end
end
