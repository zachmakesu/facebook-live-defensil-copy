namespace :admin do
  desc "admin task"

  task create_admin: :environment do
    admin = Admin.new(email: ENV['email'], password: ENV['password'])
    if admin.save
      puts "Success[✓]"
    else
      puts "Failure[✗] with errors #{admin.errors.full_messages.to_sentence}"
    end
  end
end
