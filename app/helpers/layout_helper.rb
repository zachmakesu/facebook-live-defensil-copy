module LayoutHelper
  CONTROLLER_PATH_LAYOUTS = {
    admin: %w[ admins/sessions admins/games ],
    base: []
  }.freeze

  def parent_layout(layout)
    @view_flow.set(:layout, output_buffer)
    output = render(:file => "layouts/#{layout}")
    self.output_buffer = ActionView::OutputBuffer.new(output)
  end

  def dynamic_layout(controller_path)
    layout = layout_finder(controller_path: controller_path)
    @view_flow.set(:layout, output_buffer)
    output = render(:file => "layouts/#{layout}")
    self.output_buffer = ActionView::OutputBuffer.new(output)
  end

  def layout_finder(controller_path:)
    CONTROLLER_PATH_LAYOUTS.each do |k, v|
      return "base" if k == :base
      return k.to_s if v.include?(controller_path)
    end
  end
end
