module GamesHelper
  def generate_answer_list(game)
    game.answers.decorate.each do |answer|
      concat content_tag(:li, answer.words_and_points, class: "answer_list")
    end
  end

  def generate_fetch_toggle(game)
    game.is_fetching?
  end

  def generate_answer_input(game)
    game.answers.each do |answer|
      inner_span = content_tag(
        :span, "",
        class: "ion-ios-close-empty answer-remove"
      )
      inner_tag = tag(
        "input", name: "answer[]", type: "text",
        value: answer, class: "answer", placeholder: "scope"
      )
      concat content_tag(:div, inner_span + inner_tag, class: "answer-container")
    end
  end
end
