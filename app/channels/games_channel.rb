# ActionCable Channel for Game
class GamesChannel < ApplicationCable::Channel
  def subscribed
    stream_from "games_channel"
  end

  def received
  end

  def save_comment(data)
    SaveCommentJob.perform_later(data)
  end
end
