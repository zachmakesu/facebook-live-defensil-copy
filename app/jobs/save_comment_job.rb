# Job for saving comments after trigger in webhook
class SaveCommentJob < ApplicationJob
  queue_as :default

  def perform(data)
    game_id = data["game_id"]
    data = JSON.parse(data["data"]).with_indifferent_access
    game = Game.find_by(id: game_id)
    save_comments(data: data, game: game)
  end

  private

  # rubocop:disable Metrics/AbcSize
  def save_comments(data:, game:)
    uid = data[:from][:id]
    message = data[:message].downcase
    viewer = Viewer.find_by(uid: uid)
    viewer = save_new_user(user: data[:from]) if viewer.blank?
    query = "lower(message) = ? AND game_id = ?"
    comment = viewer.comments.where(query, message, game.id).first
    new_comment_params = { viewer: viewer, message: message, game: game }
    save_new_comment(new_comment_params) if comment.blank?
  end
  # rubocop:enable Metrics/AbcSize

  def save_new_user(user:)
    viewer = Viewer.new(uid: user[:id],
                        name: user[:name],
                        profile_photo: user[:picture][:data][:url])
    viewer if viewer.save
  end

  def save_new_comment(viewer:, message:, game:)
    comment = viewer.comments.new(message: message, game_id: game.id)
    check_if_comment_is_correct(comment: comment, game: game) if comment.save
  end

  def check_if_comment_is_correct(comment:, game:)
    answer = Answer.where("lower(words) = ? AND game_id = ?",
                          comment.message.downcase, game.id).first
    ca = comment.correct_answers
    ca.create(answer_id: answer.id, game_id: game.id) if answer.present?
    answer.open_it! if answer&.open?
  end
end
