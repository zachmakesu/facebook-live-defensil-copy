# Decorator for Answer
class AnswerDecorator < Draper::Decorator
  delegate_all

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def words_and_points
    "#{object.words} (#{object.points} pts)"
  end

  def answer_status
    object.open? ? "open-answer" : "close-answer"
  end
end
