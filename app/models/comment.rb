# == Schema Information
#
# Table name: comments
#
#  created_at :datetime         not null
#  game_id    :integer
#  id         :integer          not null, primary key
#  message    :string           not null
#  updated_at :datetime         not null
#  viewer_id  :integer
#

# Model for facebook video's comments
class Comment < ApplicationRecord
  has_many :correct_answers, dependent: :destroy
  belongs_to :game
  belongs_to :viewer
end
