# == Schema Information
#
# Table name: correct_answers
#
#  answer_id  :integer
#  comment_id :integer
#  created_at :datetime         not null
#  game_id    :integer
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#

# Model for correct answer's from comments
class CorrectAnswer < ApplicationRecord
  belongs_to :answer
  belongs_to :comment
  belongs_to :game
end
