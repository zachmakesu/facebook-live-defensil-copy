# == Schema Information
#
# Table name: viewers
#
#  created_at    :datetime         not null
#  id            :integer          not null, primary key
#  name          :string
#  profile_photo :string
#  uid           :integer          not null
#  updated_at    :datetime         not null
#

# Model for video viewers
class Viewer < ApplicationRecord
  has_many :comments, dependent: :destroy
end
