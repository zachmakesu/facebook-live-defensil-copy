# == Schema Information
#
# Table name: games
#
#  access_token :string           not null
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  question     :string           not null
#  title        :string           not null
#  updated_at   :datetime         not null
#  video_id     :integer          not null
#

# Model for video live games
class Game < ApplicationRecord
  has_many :answers, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :correct_answers, dependent: :destroy
  accepts_nested_attributes_for :answers, allow_destroy: true
  validates :video_id, :access_token, presence: true

  def self.search(search)
    where("title iLIKE :search", search: "%#{search}%").order(title: :asc)
  end
end
