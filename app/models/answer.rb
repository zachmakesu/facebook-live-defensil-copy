# == Schema Information
#
# Table name: answers
#
#  created_at :datetime         not null
#  game_id    :integer
#  id         :integer          not null, primary key
#  points     :integer          default(0)
#  updated_at :datetime         not null
#  words      :string
#

# Model for game's answers
class Answer < ApplicationRecord
  has_many :correct_answers, dependent: :destroy
  belongs_to :game

  validates :words, presence: true, uniqueness: { scope: %i[words game_id] }

  def open_it!
    data = { answer_id: id, answer: words, points: points }
    ActionCable.server.broadcast "games_channel", data
  end

  def open?
    correct_answers.count >= 3
  end
end
