# Controller for Admin Game CMS
class Admins::GamesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_game, only: %i[show edit update destroy]

  def index
    @games = Game.search(params[:search]).page(params[:page]).per(10).decorate
  end

  def new
    @game = Game.new(answers: [Answer.new])
  end

  def create
    @game = Game.create(game_params)
  end

  def show
  end

  def edit
  end

  def update
    @game.update(game_params)
  end

  def destroy
    @game.destroy
  end

  private

  def set_game
    @game = Game.find(params[:id]).decorate
  end

  def game_params
    params.require(:game).permit(
      :title, :question, :video_id, :access_token,
      answers_attributes: %i[id words points _destroy]
    )
  end
end
