# Controller for Admin Game CMS
class GamesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_game, only: %i[show]

  def show
    @answers = @game.answers.decorate
  end

  private

  def set_game
    @game = Game.find(params[:id]).decorate
  end
end
