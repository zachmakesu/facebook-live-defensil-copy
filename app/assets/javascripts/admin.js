window.onload = function () {
  var timeoutID = null
  $(document).on('keyup', '#searchResources', function (e) {
    var form = $(this).parents('form:first')
    clearTimeout(timeoutID)
    timeoutID = setTimeout(function () { form.submit() }, 500)
  })

  $(document).on('change', '#game_is_fetching', function (e) {
    $(this).closest('form').submit()
  })
}

// Used in Rails View Remote True js.erb
// eslint-disable-next-line no-unused-vars
function modalShow (html) {
  // Get the modal
  var modal = document.getElementById('allModal')

  // Get Object HTML
  var objectHtml = document.getElementById('objectHtml')

  // When the user clicks on <span> (x), close the modal
  $(document).on('click', '.close', function (e) {
    $(this).parent().css('display', 'none')
  })

  $(document).click(function (e) {
    var container = $('#allModal')
    // if the target of the click isn't the container nor a descendant of the container
    if (container.is(e.target) && container.has(e.target).length === 0) {
      modal.style.display = 'none'
    }
  })

  modal.style.display = 'block'
  objectHtml.innerHTML = html
  $('input[tabindex=0]').focus()
}
