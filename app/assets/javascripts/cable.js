// Action Cable provides the framework to deal with WebSockets in Rails.
// You can generate new channels where WebSocket features live using the `rails generate channel` command.
//
//= require action_cable
//= require_self
//= require_tree ./channels

(function () {
  this.App || (this.App = {})
  var websocketURI
  websocketURI = function () {
    var prot
    prot = location.protocol === 'https:' ? 'wss:' : 'ws:'
    return prot + '//' + location.host + '/cable/'
  }
  App.cable = ActionCable.createConsumer(websocketURI())
}).call(this)
